{
  description = "guiHaskell";


  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, }:
    let utils = flake-utils.lib;
    in utils.eachDefaultSystem (system:
      let
        compilerVersion = "ghc902";
        pkgs = nixpkgs.legacyPackages.${system};
        hsPkgs = pkgs.haskell.packages.${compilerVersion}.override {
          overrides = hfinal: hprev: {
            guiHaskell = hfinal.callCabal2nix "guiHaskell" ./. { };
          };
        };
      in rec {
        packages = utils.flattenTree { guiHaskell = hsPkgs.guiHaskell; };


        devShell = hsPkgs.shellFor {
          withHoogle = true;
          packages = p: [ p.guiHaskell ];
          buildInputs = [
            # pkgs.cabal2nix
            pkgs.cabal-install
            pkgs.hlint
            pkgs.ormolu
            pkgs.haskell.compiler."${compilerVersion}"
            hsPkgs.ghcid
            # hsPkgs.haskell-language-server
            # hsPkgs.alex
            # hsPkgs.happy
            # hsPkgs.hspec-discover
          ];
          # ++ (builtins.attrValues (import ./scripts.nix {s = pkgs.writeShellScriptBin;}));
        };

        defaultPackage = packages.guiHaskell;
      });
}
