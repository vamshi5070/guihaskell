{-# LANGUAGE OverloadedStrings #-}
import GI.Gtk hiding (on,(:=),init)
import qualified GI.Gtk as Gtk (init)
import GI.Gtk.Declarative

main :: IO ()
main = do
    Gtk.init Nothing
    window <- windowNew
    set window [windowDefaultWidth := 600, windowDefaultHeight := 400, windowTitle := "Text Editor"]
    textView <- textViewNew
    containerAdd window textView
    on window objectDestroy mainQuit
    widgetShowAll window
    mainGUI
